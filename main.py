from flask import Flask, render_template, request, jsonify, send_from_directory
import RPi.GPIO as GPIO
from datetime import datetime, timedelta
import threading
import time

app = Flask(__name__, static_url_path='/static')

# Configuration
CAMERA_PIN = 4  # GPIO pin number
TRIGGER_DURATION = 0.5  # seconds

# Set up GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(CAMERA_PIN, GPIO.OUT)
GPIO.output(CAMERA_PIN, GPIO.LOW)

# Global variables
capture_frequency = 60  # default: 1 minute
timelapse_duration = 3600  # default: 1 hour
is_capturing = False
capture_count = 0
last_capture_time = None
capture_thread = None

def trigger_camera():
    GPIO.output(CAMERA_PIN, GPIO.HIGH)
    time.sleep(TRIGGER_DURATION)
    GPIO.output(CAMERA_PIN, GPIO.LOW)

def capture_images():
    global capture_count, last_capture_time, is_capturing
    start_time = datetime.now()
    end_time = start_time + timedelta(seconds=timelapse_duration)

    while datetime.now() < end_time and is_capturing:
        # Trigger the camera
        trigger_camera()

        capture_count += 1
        last_capture_time = datetime.now()

        # Wait for the next capture
        time.sleep(capture_frequency - TRIGGER_DURATION)

    is_capturing = False

@app.route('/')
def index():
    return render_template('index.html', 
                           capture_frequency=capture_frequency,
                           timelapse_duration=timelapse_duration,
                           capture_count=capture_count,
                           last_capture_time=last_capture_time,
                           is_capturing=is_capturing)

@app.route('/start', methods=['POST'])
def start_capture():
    global capture_frequency, timelapse_duration, is_capturing, capture_thread

    if is_capturing:
        return jsonify({"status": "error", "message": "Capture already in progress"})

    capture_frequency = int(request.form.get('frequency', 60))
    timelapse_duration = int(request.form.get('duration', 3600))

    is_capturing = True
    capture_thread = threading.Thread(target=capture_images)
    capture_thread.start()

    return jsonify({"status": "success", "message": "Capture started"})

@app.route('/stop', methods=['POST'])
def stop_capture():
    global is_capturing

    if not is_capturing:
        return jsonify({"status": "error", "message": "No capture in progress"})

    is_capturing = False
    return jsonify({"status": "success", "message": "Capture stopped"})

@app.route('/status')
def get_status():
    return jsonify({
        "is_capturing": is_capturing,
        "capture_count": capture_count,
        "last_capture_time": last_capture_time.isoformat() if last_capture_time else None
    })

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

if __name__ == '__main__':
    try:
        app.run(host='0.0.0.0', port=5000, debug=True)
    finally:
        GPIO.cleanup()  # Ensure GPIO is cleaned up when the server exits